#pragma once
/**
 * This is adoption of nice technique for automatic treatment of resources at
 * the end of scope. Original article comes from:
 * [1] http://www.cuj.com/documents/s=8000/cujcexp1812alexandr/
 *
 * ScopeGuard is a generalization of a typical implementation of the
 * "initialization is resource acquisition" C++ idiom.  The difference is that
 * ScopeGuard focuses only on the cleanup part - you do the resource
 * acquisition, and ScopeGuard takes care of relinquishing the resource.
 */

/**
 * How to use it:
 *      {
 *          bb::scope_guard_t guard = bb::mkScopeGuard(bb::mk_functor(&log_leave));
 *          if (something goes wrong)
 *              throw std::exception; // the guard will call log_leave
 *          else
 *              guard.Dismiss();
 *      }
 *
 *      or simply
 *      {
 *          SCOPE_GUARD(guard, boost::bind(&foo::bar, f, true));
 *          ...
 *          guard.Dismiss(); // optional
 *      }
 *
 *      This code will construct a temporary reference guard which will be destroyed
 *      at the exit of the scope and call log_leave function.
 *
 *      The guard can be revoked by calling its member method Dismiss. 
 */
#include <tuple>
#include <functional>

namespace detail {
template <class F, class Tuple, std::size_t... I>
constexpr decltype(auto) apply_impl(F&& f, Tuple&& t, std::index_sequence<I...>)
{
    return std::invoke(std::forward<F>(f), std::get<I>(std::forward<Tuple>(t))...);
}
}  // namespace detail

template <class F, class Tuple>
constexpr decltype(auto) applyX(F&& f, Tuple&& t)
{
    return detail::apply_impl(
        std::forward<F>(f), std::forward<Tuple>(t),
        std::make_index_sequence<std::tuple_size_v<std::remove_reference_t<Tuple>>>{});
}

namespace bb { namespace detail {

	struct ScopeGuardBase
	{
		void Dismiss () const { m_Dismissed = true; }
	protected:
		ScopeGuardBase () : m_Dismissed(false) {}
		ScopeGuardBase (ScopeGuardBase const & rhs) : m_Dismissed(rhs.m_Dismissed) { rhs.Dismiss(); }
		~ScopeGuardBase () { } // nonvirtual! see [1] why
		mutable bool m_Dismissed;
	private:
		ScopeGuardBase & operator= (ScopeGuardBase const &); // assignment denied
	};

	template <class _FuncT, typename... _ArgsT>
	struct ScopeGuard : ScopeGuardBase
	{
		std::tuple<_ArgsT...> m_args;

		ScopeGuard (_FuncT const & fn, _ArgsT && ... arg) : m_fn(fn), m_args(std::forward<_ArgsT>(arg)...) { }

		~ScopeGuard ()
		{
			if (!m_Dismissed)
			{
				/*std::invoke(
						[this] (const auto & ... item)
						{
							m_fn(item...);
						},
					m_args);*/

				std::apply(m_fn, m_args);
			}
		}

	private:
		_FuncT m_fn;
	};

}} // namespace bb::detail


namespace bb {

	typedef detail::ScopeGuardBase const & scope_guard_t;

	/*template <class _FunctorT>
	detail::ScopeGuard_0<_FunctorT> mkScopeGuard (_FunctorT const & f)
	{
		return detail::ScopeGuard_0<_FunctorT>(f);
	}*/

	template <class _FuncT, typename... _ArgsT>
	detail::ScopeGuard<_FuncT, _ArgsT...> mkScopeGuard (_FuncT const & fn, _ArgsT &&... args)
	{
		return detail::ScopeGuard<_FuncT, _ArgsT...>(fn, std::forward<_ArgsT>(args)... );
	}

	/*template <class _ResultT, class _ObjectT, typename... _ArgsT>
	detail::ScopeGuardMemFun<_ResultT, _ObjectT, _ArgsT...> mkScopeGuard (_ResultT (*fn) (_ArgsT...), _ObjectT * obj, _ArgsT &&... args)
	{
		return detail::ScopeGuardMemFun<_ResultT, _ObjectT, _ArgsT...>(fn, obj, std::forward<_ArgsT>(args)... );
	}*/


} // namespace bb

