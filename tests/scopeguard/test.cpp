#include <bblib/ScopeGuard.h>

//#define DO_BOOST_BIND_TESTS
#if defined DO_BOOST_BIND_TESTS
#	include <boost/bind.hpp>
#	include <boost/lambda/lambda.hpp>
#	include <boost/lambda/bind.hpp>
#endif

#include <functional>
#include <iostream>
#include <cassert>
#define BB_ASSERT assert

#if defined _MSC_VER
#	define __PRETTY_FUNCTION__ __FUNCSIG__ 
#endif

void bar_fn ()
{
	std::cout << " called " << __PRETTY_FUNCTION__ << std::endl;
}

void bar_fn_1 (int aa)
{
	std::cout << " called " << __PRETTY_FUNCTION__ << std::endl;
}



struct foo
{
	void bar () const { std::cout << " called " << __PRETTY_FUNCTION__ << std::endl; }
	void bar1 (int) const { std::cout << " called " << __PRETTY_FUNCTION__ << std::endl; }
	void bar2 (int, int) const { std::cout << " called " << __PRETTY_FUNCTION__ << std::endl; }
	void bar3 (int, int, int) const { std::cout << " called " << __PRETTY_FUNCTION__ << std::endl; }
};



int main ()
{
	{
		std::cout << "pointer to function test\n";
		bb::scope_guard_t tmp = bb::mkScopeGuard(&bar_fn);
	}

	{
		std::cout << "pointer to function with arg test\n";
		bb::scope_guard_t tmp = bb::mkScopeGuard(&bar_fn_1, 1024);
	}

	{
		std::cout << "pointer to member fun test\n";
		foo f;
		bb::scope_guard_t tmp0 = bb::mkScopeGuard(&foo::bar, &f);
		bb::scope_guard_t tmp1 = bb::mkScopeGuard(&foo::bar1, &f, 1);
		bb::scope_guard_t tmp2 = bb::mkScopeGuard(&foo::bar2, &f, 1, 2);
	}

	/*{
		std::cout << "mem_fun 1 Dismiss test\n";
		foo f;
		bb::scope_guard_t tmp = bb::mkScopeGuard(std::mem_fun(&foo::bar_1), &f, 1);
		tmp.Dismiss();
	}*/
}
