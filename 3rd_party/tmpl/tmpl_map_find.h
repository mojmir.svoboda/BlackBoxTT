#pragma once
#include <functional>
#include <type_traits>

template<class M, class K> struct tmpl_map_find_impl;

template<class M, class K>
using tmpl_map_find = typename tmpl_map_find_impl<M, K>::type;

template<template<class...> class M, class... T, class K>
struct tmpl_map_find_impl<M<T...>, K>
{
  struct U : std::type_identity<T>... { };

  template<template<class...> class L, class... U>
  static std::type_identity<L<K, U...>> f( std::type_identity<L<K, U...>>* );
  static std::type_identity<void> f( ... );

  using V = decltype( f((U*)0) );
  using type = typename V::type;
};

